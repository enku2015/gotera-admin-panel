/**
 * Created by enkuwendwosen on 4/18/16.
 */
angular.module('GoteraAdminPanel').service('dataService',function(Upload){

    var dataService = {};

    dataService.createContentProviderAccount = function(){

        var response = {};
        var baseUrl = 'localhost:8080';
        Upload.upload({
            url: baseUrl + '/accounts/createUserAccount',
            method: 'POST',
            sendFieldsAs: 'form',
            fields: {
                username : "enku",
                password : "enku",
                email    : "enku.wende@gmail.com",
                phoneNumber : "12122121",
                conf_type  : 1212
            }
        }).then(function (resp) {
           response = resp;
        });
        return response;
    };
    return dataService;

});