/**
 * Created by enkuwendwosen on 4/18/16.
 */
angular.module('GoteraAdminPanel').
directive('wgFab',function() {
    return {
        templateUrl: 'views/widgets/wg-fab.html',
        restrict: 'E',
        replace: true,
        scope: {
            id: '@wgId',
            title: '@wgTitle',
            description: '@wgDescription',
            Filter: '=wgFilter',
            page : '@wgPage'
        },
        controller: function ($scope) {
            $scope.FABs = [{
                src: "assets/svg/ai-event.svg",
                location: 'events'
            },{
                src: "assets/svg/ai-add-to-queue.svg",
                location: 'feeds'
            },{
                src: "assets/svg/ai-upload.svg",
                location: 'contents'
            }];
            $scope.CurrentFAB = _.where($scope.FABs,{location:$scope.page});
        }
    }
});