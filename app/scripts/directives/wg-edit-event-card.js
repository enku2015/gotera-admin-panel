/**
 * Created by aman on 4/29/16.
 */
angular.module('GoteraAdminPanel').
    directive('wgEditEventCard', function () {
        return{
            templateUrl:'views/widgets/wg-edit-event-card.html',
            restrict:'E',
            replace:true,
            scope: {
                eventId : '@id'
            },
            link: function($scope, $elem, $attrs) {
                console.log('test', $scope.eventId);
            },
            controller : function ($scope,$timeout) {
                this.eventIdVal = $scope.eventId;
                $scope.upload = function () {
                    console.log("Test...........");
                    $timeout(function() {
                        angular.element(document.getElementById('fileInput')).triggerHandler('click');
                    }, 0);
                };
            }
        }
    }
);