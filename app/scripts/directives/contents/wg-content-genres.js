/**
 * Created by enkuwendwosen on 5/3/16.
 */
angular.module('GoteraAdminPanel').directive('wgContentGenres',function(){
    return{
        templateUrl:'views/widgets/content/wg-content-genres.html',
        restrict:'E',
        replace:true,
        controller : function ($scope) {

        }
    }
})