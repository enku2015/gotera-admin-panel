/**
 * Created by enkuwendwosen on 4/19/16.
 */
angular.module('GoteraAdminPanel').directive('wgContentArtists',function(){
    return{
        templateUrl:'views/widgets/content/wg-content-artist.html',
        restrict:'E',
        replace:true,
        controller : function ($scope) {

        }
    }
})