/**
 * Created by enkuwendwosen on 4/19/16.
 */
angular.module('GoteraAdminPanel').directive('wgContentAlbums',function(){
    return{
        templateUrl:'views/widgets/content/wg-content-albums.html',
        restrict:'E',
        replace:true,
        controller : function ($scope) {

        }
    }
})