/**
 * Created by enkuwendwosen on 5/2/16.
 */
angular.module("GoteraAdminPanel").
    directive("wgToolbar",function(){
    return{
        templateUrl:'views/widgets/wg-toolbar.html',
        restrict:'E',
        replace:true,
        controller : function ($scope,$timeout,toastr) {
           $scope.showNotifications = function(){
               var i = 0;
               for(i ; i < 10 ; i++){
                    toastr.info('Are you the 6 fingered man?');
               }
           }
           $scope.$on("inLogin",function(){
                $scope.show = false;
           });
           $scope.$on("outOfLogin",function(){
               $scope.show = true;
           });
        }
    }
})