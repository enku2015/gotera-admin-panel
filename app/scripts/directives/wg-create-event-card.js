/**
 * Created by aman on 4/24/16.
 */
angular.module('GoteraAdminPanel').
    directive('wgCreateEventCard', function () {
        return{
            templateUrl:'views/widgets/wg-create-event-card.html',
            restrict:'E',
            replace:true,
            link: function($scope, $elem, $attrs) {
            },
            controller : function ($scope,$timeout) {
                $scope.upload = function () {
                    console.log("Test...........");
                    $timeout(function() {
                        angular.element(document.getElementById('#fileInput')).triggerHandler('click');
                    }, 0);
                };
            }
        }
    }
);