/**
 * Created by enkuwendwosen on 5/2/16.
 */
angular.module("GoteraAdminPanel").
directive("wgSideNavigation",function(){
    return{
        templateUrl:'views/widgets/wg-side-navigation.html',
        restrict:'E',
        replace:true,
        controller : function ($scope,$timeout) {
            $scope.$on("inLogin",function(){
                $scope.show = false;
                $scope.flex = 100;
            });
            $scope.$on("outOfLogin",function(){
                $scope.show = true;
                $scope.flex = 80;
            });
        }
    }
})