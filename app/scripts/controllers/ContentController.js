/**
 * Created by enkuwendwosen on 5/3/16.
 */
angular.module("GoteraAdminPanel")
    .controller("ContentController",function($scope,$mdDialog,$mdMedia,dataService){

        $scope.showUploadDialog = function(ev) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
            $mdDialog.show({
                    controller: UploadResourceDialogController,
                    templateUrl: 'views/dialogs/upload-resource.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: useFullScreen
                })
                .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
            $scope.$watch(function() {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function(wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
            });
            function UploadResourceDialogController($scope){
                $scope.hide = function() {
                    $mdDialog.hide();
                };
                $scope.cancel = function() {
                    $mdDialog.cancel();
                };
            }
        };
        console.log(dataService.createContentProviderAccount());
    });