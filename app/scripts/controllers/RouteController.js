/**
 * Created by enkuwendwosen on 4/16/16.
 */
angular.module('GoteraAdminPanel').controller("RouteController",function($scope,$location){
    $scope.Route = function(to){
        console.log(to)
        if (to == '') return;
        $location.path = "/"+to;
    }
});