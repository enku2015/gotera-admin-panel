/**
 * Created by enkuwendwosen on 4/3/16.
 */
angular.module('GoteraAdminPanel',['ngMaterial','ngRoute', 'mdPickers','ui.bootstrap', 'ngAnimate','toastr','md.data.table','ngFileUpload'])
    .config(function($mdThemingProvider) {
        $mdThemingProvider.theme('default')
            .primaryPalette('green')
            .accentPalette('light-green');
    })
    .config(['$routeProvider',function($routeProvider){
        $routeProvider.when('/content',{
            title: 'Content',
            templateUrl : 'views/content.html',
            controller : 'ContentController'
        }).when('/accounts',{
            title:'Accounts',
            templateUrl : 'views/accounts.html',
            controller : 'AccountsController'
        }).when('/login',{
            title:'Login',
            templateUrl : 'views/login.html',
            controller : 'LoginController'
        }).otherwise({
            redirectTo : '/content'
        });
    }]).run(function($rootScope,$location,$window){
        $rootScope.$on("$routeChangeStart",function(event,next,current){
            $rootScope.authenticated = false;
            if($window.sessionStorage['userInfo'] != null){

            }else{
                var nextUrl = next.$$route.originalPath;
                if (nextUrl == '/login') {

                }else {
                    $location.path("/login");
                }
            };
    });
});